numerateur = 7
denominateur = 34
try:
    resultat = int(numerateur) / int(denominateur)
except NameError as variable_non_définie:
    print("La variable numerateur ou denominateur n'est pas initialisée:", variable_non_définie)
except TypeError:
    print("la variable numerateur ou denominateur posséde un type incompatible avec la division")
except ZeroDivisionError:
    print("La variable denominateur est égale à 0")
else:
    print("Le resulat obtenu est:", resultat)