ma_listeone = list(

)
print("ListoneInit:")
print(ma_listeone, "\n")

ma_listeoneBis = ma_listeone.append("toto")
print("ListoneInitAppended:")
print(ma_listeone, "\n")
print("Let check what's inside the var used to append ma_listeoneInit:")
print("ma_listeoneInitBis:")
print(ma_listeoneBis, "\n")
print("NB: So, unlike method_chain, method_list has origin_object_modification_capabilities\n")
print("None: is a python empty object\n")


ma_listetwo = []

print("ListetwoInit:")
print(ma_listetwo, "\n")

ma_listethree = [1,2,3,4,5]

print("ListethreeInit:")
print(ma_listethree, "\n")

ma_listefour = [1,2.3, "une chaine", []]

print("ListfourInit:")
print(ma_listefour, "\n")

print("ListfourAccessID0: On accède au premier élément de la liste")
print(ma_listefour[0],"\n")
print("ListfourAccessID2: On accède à au troisième élément")
print(ma_listefour[2],"\n")

ma_listefour[1] = 'Z'

print("ListefourReplaceID1:On remplace le deuxième elt par Z car les liste à la différence des chaines sont de types mutables")
print(ma_listefour,"\n")

#Insertion d'objet dans une liste
"""On ajoute 54 à la fin de la liste. A la différence des chaines, ici, on modifie bien l'objet
si on passe par un deuxième objet(variable) pour ajouter un élément à la liste, le print de ce dernier renvoie None"""

ma_listefour.append(54)

print("ListfourAppended: On ajoute un un elt dans la liste")
print(ma_listefour,"\n")

ma_listefour.insert(0, 'c')

print("ListfourInsertion: On insert une lettre à l'indice 0 en observant un décalage")
print(ma_listefour,"\n")

#Concaténation de listes
ma_listethree.extend(ma_listefour)# On insére la liste four dans la liste three

print("ListthreeExtendedWithListfour: Extend with extend method")
print(ma_listethree,"\n")

ma_listeConcat = ma_listethree + ma_listefour

print("ListthreeListfourConcat: la liste three est précédemment rallongée ")
print(ma_listeConcat,"\n")

ma_listethree += ma_listefour # Pareil que l'extend

print("ListthreeExtendedWithListfour: Extend with += method")
print(ma_listethree,"\n")

#Suppression d'élément d'une liste
del ma_listethree[7]
""" On supprime le 8 eme élément(Z) de la liste par del qui peut aussi être utilisé pour supprimer
 une variable ou élément d'une séquence"""

print("listthree: On Supprime l'elt ID7 de la liste three, usage du MOT-CLE(del)")
print(ma_listethree,"\n")

ma_listethree.remove(54)# On supprime 54 de la liste. Cette fonction ne retire que la première occurence trouvée

print("listthree: On Supprime la 1ere occurrence 54 de la liste three, usage de la METHODE(remove)")
print(ma_listethree,"\n")

#Parcourir une liste avec une boucle while
print("Listthree: Premier Parcourt BOUCLE(while):")
i = 0
while i < len(ma_listethree):

    print(ma_listethree[i],"\n")
    i +=1

print("Listthree: Deuxième Parcourt BOUCLE(for):")
#Parcourir une liste avec une boucle for
for elt in ma_listethree:
    print(elt)

print("\n")
print("Listethree: Troisième parcourt FUNCTION(enumerate:)")
#Parcour une liste avec la fonction enumerate
for i, elt in enumerate(ma_listethree):
    print(
        "A l'indice {} se trouve {}.".format(i, elt)
    )
print("\n")
print("Listethree: Affichage  de l'indice tjrs avec FONCTION(enumerate_améliorée):\n")
print("Here we get a tuple, a pair of clue & object.")
for elt in enumerate(ma_listethree):
    print(elt,"\n")