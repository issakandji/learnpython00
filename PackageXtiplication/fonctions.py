def table_de_multiplication(nb, max=10):
    """Fonction affichant la table de multiplication de valeur de 1 à max
    max >= 10"""
    i = 0
    while i < max:
        print(nb, "*" , i + 1, "=", nb * (i + 1))
        i += 1


# Test de la fonction table
if __name__ == '__main__': # Ce code ne s'execute qu'à travers ce script et ne peux être exécuter en import
    table_de_multiplication(4)

#Test de la docu
help(table_de_multiplication)
