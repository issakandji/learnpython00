from random import randrange # Nous générons un nombre aléatoire entre 0 et 49
from FonctionWin import Gagne # Arondir
from FonctionWin import gagne

mise_Global = 500  # type: int
le_Jeu_Se_Poursuit = True
print("Vous êtes installé à la table de jeu avec une mise de", mise_Global, "$")


while le_Jeu_Se_Poursuit:

# l'organisateur du jeu demande au joueur de choisir un numéro
    numero = -1
    while numero < 0 or numero > 49:
        numero = input("Veuillez faire votre choix de numéro entre 0 et 49:")
        try:
            numero = int(numero)
        except ValueError:
            print("Introduiser un numéro compris en 0 et 49")
            numero = -1
            continue
        if numero < 0 or numero > 49:
            print("le numéro doit être compris entre 0 et 49")

#Le joueur donne sa mise sur le numéro choisi
    mise = 0
    while mise <= 0 or mise > mise_Global :
        mise = input("Donner votre mise sur le numéro choisi:")
        try:
            mise = int(mise)
        except ValueError:
            print("Donner une mise comprise en 1 et", mise_Global)
            mise = 0
            continue
        if mise <= 0 or mise > mise_Global:
            print("Votre mise doit être comprise entre 1 et", mise_Global)

#On fait tourner la roue
    numero_Gagnant = randrange(50)
    print("Le numero sorti est le:", numero_Gagnant)

    if numero != numero_Gagnant and ((numero%2 == numero_Gagnant%2 == 0) or (numero%2 == numero_Gagnant%2 != 0)):
        print("Votre jeu de couleur est bon et vous avez gagné:", gagne(mise))
        mise_Global += gagne(mise)
        print("Vous avez:",mise_Global, "dans votre pot")


    elif numero == numero_Gagnant:
        print("Le numéro tiré est gagnant et vous gagnez:", Gagne(mise))
        mise_Global += Gagne(mise)
        print("Vous avez", mise_Global, "dans votre pot")


    else:
        print("Vous avez perdu:", "choix:", numero, "tirage:", numero_Gagnant)
        mise_Global -= mise
        print("Vous avez", mise_Global, "dans votre pot")

#Vérifions si le joueur continue la partie

    if mise_Global <= 0:
        print("Vous n'avez plus de sous, le jeu est terminé pour vous")
        le_Jeu_Se_Poursuit = False

    else:
        print("vous avez une mise globale de", mise_Global, "$")
        quitter = input("Souhaitez vous quitter la partie ? Y pour quitter et ENTER pour pousuivre ?")

        if quitter == "y" or quitter == "Y":
            print("Le jeu est terminé")
            le_Jeu_Se_Poursuit = False
