minuscules = "une chaine de minuscules"
print(minuscules.upper()) # Mettre en majuscules
print(minuscules.capitalize()) # La première lettre en majuscule
espaces = "  une  chaine  avec  des  espaces  "
print(espaces.strip()) # Retire les espaces au début et à la fin
titre = "introduction"

""" Mettre en majuscule et centrer avec une longeur de 20 
ajout d'espaces 4 au début et 4 à la fin pour atteindre la longueur demandée """

titreMaj = titre.upper()
print(titreMaj.center(20))



print(titre.upper().center(20))
"""La methode upper travaille sur la chaine renvoyée par la methode upper"""

prenom = "Issa"
nom = "Kandji"
age = 44
print("Je m'apelle {0} {1} et j'ai {2} ans.".format(prenom, nom, age))
"""Nombre de d'accolades égale nombre de variables dans la methode format"""
print("Je m'appelle {0} {1} ({3} {0} pour l'administration) et j'ai {2} ans".format(prenom, nom, age, nom.upper()) )

"""Utilisation de la method format sans spécifier les numéros de variable"""
date = "Samedi 12 Janvier 2019"
heure = "13:25"
print("Cela s'est produit le {}, à {}.".format(date, heure))

"""Utilisation des noms de variables dans la methode format, à la place des numéros de variables"""
adresse = "{no_rue}, {nom_rue}, {code_postal} {nom_ville} ({pays})".format(no_rue=1172 \
,nom_rue="Route de Saint Soulan"
, code_postal=32130, nom_ville="Samatan"
,pays="France")
print(adresse,"\n")


"""Concaténation de chaines"""
chaine_complete = prenom + nom + adresse
print(chaine_complete, "\n")
"""Insertion d'espaces"""
chaine_complete = prenom + " " + nom +":"+ adresse
print(chaine_complete)

"""Conversion de int en str"""
age = 44
message = "j'ai " + str(age) + " ans."
print(message)



