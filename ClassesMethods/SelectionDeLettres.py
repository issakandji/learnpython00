#Selection de lettres dans une chaine
presentation = "salut"
print(presentation[0:2]) # On selectionne les deux premières lettres
print(presentation[2:len(presentation)])#Selection de tout sauf les deux premières lettres
print(presentation[:2]) # Selection du début jusqu'à la troisième lettre non comprise
print(presentation[2:]) # Sélection à partir de la troisième lettre (comprise) jusqu'à la fin