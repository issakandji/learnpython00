chaine = "Salut les ZEROS !"
chaine[0]# premier lettre de la chaine
print(chaine[0])
chaine[2]# Troisième lettre de la chaine
print(chaine[2])
chaine[-1]#Dernière lettre de la chaine
print(chaine[-1])
# Avant-avant dernière lettre de la chaine
print(chaine[-3],"\n")
#Longeur de la chaine
print(len(chaine),"\n")
#Parcours par une boucle while
chaine = "Salut les ZEROS !"
i = 0
while i < len(chaine):
    print(chaine[i])
    i += 1


