#Pas de changement de place de lettre en utilisant les indices. Utiliser plutôt la selection
mot = "lac"
#mot[0] = "b" # On veut remplacer "l" par "b"

#Solution en employant la selection
mot = "b" + mot[1:]
print(mot)