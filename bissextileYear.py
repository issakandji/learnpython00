#!/usr/bin/python3.4
# -*-coding:utf-8 -*
#Programme testant si une année saisie par l'utilisateur est bissextile ou non
annee = input("Saisissez une année:")


try:
    annee = int(annee)
    assert annee > 0
except ValueError:
    print("Error when converting year. User encoded a string")
except AssertionError:
    print("L'année saisie n'est pas supérieur à Zéro")

#bissextile = False

if annee % 400 == 0 or (annee % 4 == 0 and annee % 100 != 0):
    print("Annee Bissextile")

else:
    print("l'annee n'est pas bisextile")
