#!/usr/bin/python3.4
# -*-coding:utf-8 -*
#Programme testant si une année saisie par l'utilisateur est bissextile ou non
annee = input("Saisissez une année:")

try:
    annee = int(annee)
    if annee <= 0:
        raise ValueError("L'année saisie n'est pas strictement supérieur à Zéro")
except ValueError:
    print("La valeur saisie est invalide (l'année est peut être négative")

#bissextile = False

if annee % 400 == 0 or (annee % 4 == 0 and annee % 100 != 0):
    print("Annee Bissextile")

else:
    print("l'annee n'est pas bisextile")